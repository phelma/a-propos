# Phelma (gricad-gitlab)

Ce groupe a pour but de regrouper les projets internes de Phelma :

* Conception des travaux pratiques et BE
* Documentations
* Projets enseignants ou encadrants
* Projets de l'Assistance à l'Enseignement


La majorité des projets sont accessibles en lecture aux utilisateurs connectés sur la plateforme.

Vous pouvez contacter les administrateurs du groupe pour demander accès en écriture à un ou plusieurs dépôts ou sous-groupes :

* Félix Piédallu <felix.piedallu@grenoble-inp.fr>
* Nicolas Ruty <nicolas.ruty@grenoble-inp.fr>
* Julien Traveaux <julien.traveaux@grenoble-inp.fr>
